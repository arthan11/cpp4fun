/*************************
 -=[ program kolorki 4 ]=-

 prezentuje napis (znak po znaku) w losowych miejscach
 kazda litera ma losowy kolor
 program dziala do czasu gdy uzytkownik nacisnie dowolny klawisz

 *************************/
#include <iostream>
#include <conio2.h>
#include <string>

using namespace std;

const string TEKST = "NACISNIJ DOWOLNY KLAWISZ";

int Napis(string txt)
    {
    int x = rand()%25 +1;
    int y = rand()%(80-txt.length()) +1 ;
    gotoxy(y, x); // przesuniecie kursora

    for (int i=0; i<txt.length(); i++)
       {
       textcolor(rand()%15);  // ustawienie koloru tekstu       
       cout << txt[i]; // wypisanie napisu
       _sleep(100);

       if (kbhit())
          break;
       }
    }

int RysujTlo(int ileramek)
   {
//   textbackground(rand()%16);  // ustawienie koloru tla
   clrscr();
   }

int main()
   {
   srand(time(NULL));  // generator liczb losowych
   textbackground(15);

   while(!kbhit()) // w petli do nacisniecia dowolnego klawisza
      {
      RysujTlo(11);
      Napis(TEKST);
      _sleep(50); // pauza
      }

   return 0;
   }
