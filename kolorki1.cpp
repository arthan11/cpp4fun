/*************************
 -=[ program kolorki 1 ]=-

 prezentuje na srodku napis, kolor tla i tekstu zmienia sie losowo
 program dziala przez okreslona przez uzytkownika ilosc powtorzen

 *************************/
#include <iostream>
#include <conio2.h>

using namespace std;

int main()
    {

    int KolTla, KolTxt, n; // deklaracja zmiennych

    srand(time(NULL)); // generator liczb losowych

    cout << "Podaj ilosc powtorzen: ";
    cin >> n;   // pobranie ilosci powtorzen

    for (int i=0; i<n ; i++) // w petli od 0 do n-1
        {
        KolTla = rand()%16; // wylosowanie koloru tla
        textbackground(KolTla);  // ustawienie koloru tla
        clrscr(); // czysci ekran

        KolTxt = rand()%16; // wylosowanie koloru tekstu
        textcolor(KolTxt);  // ustawienie koloru tekstu

        gotoxy(34,13); // przesuniecie kursora
        cout << "HELLO WORLD" << endl; // wypisanie napisu

        _sleep(50); // pauza
        }


    getch();
    return 0;
    }
