/*************************
 -=[ program kolory]=-

 wyswietla tablice kolorow, ich numery i stale uzywane w C
 program dziala do czasu gdy uzytkownik nacisnie dowolny klawisz

 *************************/

#include <iostream>
#include <conio2.h>

using namespace std;

int main()
   {
   cout << "\n\n\n\n";  // przesuniecie bardziej w dol

   for (int i=0; i < 16; i++)
      {
      cout.width(30); // przysuniecie bardziej na srodek
      cout << right << i << " - "; // wypisanie numery

      textbackground(i); // ustawienie koloru tla
      cout << "     "; // narysowanie tla

      textbackground(0); // przywrocenie koloru czarnego
      switch(i) // wypisanie odpowiedniego napisu
         {
         case 0: cout << " - BLACK"; break;
         case 1: cout << " - BLUE"; break;
         case 2: cout << " - GREEN"; break;
         case 3: cout << " - CYAN"; break;
         case 4: cout << " - RED"; break;
         case 5: cout << " - MAGENTA"; break;
         case 6: cout << " - BROWN"; break;
         case 7: cout << " - LIGHTGRAY"; break;
         case 8: cout << " - DARKGRAY"; break;
         case 9: cout << " - LIGHTBLUE"; break;
         case 10: cout << " - LIGHTGREEN"; break;
         case 11: cout << " - LIGHTCYAN"; break;
         case 12: cout << " - LIGHTRED"; break;
         case 13: cout << " - LIGHTMAGENTA"; break;
         case 14: cout << " - YELLOW"; break;
         case 15: cout << " - WHITE"; break;
         }

      cout << endl; // po kazdym kolorze przejscie do nastepnej linii
      }

   getch();  // czeka na nacisniecie klawisza
   }
