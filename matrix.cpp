/*************************
 -=[ program MATRIX ]=-

 MATRIXowe znaczki
 program dziala do czasu gdy uzytkownik nacisnie dowolny klawisz

 Autor : Krzysztof Lemka
 *************************/
#include <iostream>
#include <conio2.h>
#include <string>

using namespace std;

// parametry programu
const bool CzyANSII    = true;
const int prPoPustym   = 2;
const int prPoZnaku    = 98;
const int spowolnienie = 100;

string Linia;

int InitLinia()
   {
   Linia = "";
   for (int i=1; i<80; i++)
      Linia += ' ';
   }

int DrawLinia()
   {
   int prawdopod;

   for (int i=0; i<Linia.length(); i++)
      {
      if (Linia[i+1]==' ')
         prawdopod = prPoPustym;
      else
         prawdopod = prPoZnaku;

      if (rand()%100 >= prawdopod)
         Linia[i+1] = ' ';
      else
         if (CzyANSII)
            Linia[i+1] = 32 + rand()%(256-32); //znaki ANSII
         else
            Linia[i+1] = '0' + rand()%2; //tylko 0 i 1
      }

   cout << Linia;
   }


int main()
   {
   srand(time(NULL));  // generator liczb losowych
   textcolor(GREEN); // kolor tla
   InitLinia();

   while(!kbhit()) // w petli do nacisniecia dowolnego klawisza
      {
      gotoxy(1,1);
      insline();
      DrawLinia();
      _sleep(spowolnienie); // pauza
      }
   return 0;
   }
