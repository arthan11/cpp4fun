/*************************
 -=[ program kolorki 2 ]=-

 prezentuje na srodku napis, kolor tla i tekstu zmienia sie losowo
 program dziala do czasu gdy uzytkownik nacisnie jakis klawisz

 *************************/
#include <iostream>
#include <conio2.h>

using namespace std;

int main()
    {

    int KolTla, KolTxt; // deklaracja zmiennych

    srand(time(NULL));  // generator liczb losowych

    while(!kbhit()) // w petli do nacisniecia dowolnego klawisza
        {
        KolTla = rand()%16; // wylosowanie koloru tla
        textbackground(KolTla);  // ustawienie koloru tla
        clrscr(); // czysci ekran

        KolTxt = rand()%16; // wylosowanie koloru tekstu
        textcolor(KolTxt);  // ustawienie koloru tekstu

        gotoxy(34,13); // przesuniecie kursora
        cout << "HELLO WORLD" << endl; // wypisanie napisu

        _sleep(50); // pauza
        }

    getch();     // po kbhit()
    getch();     // czeka na nacisniecie klawisza
    return 0;
    }
