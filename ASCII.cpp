/*************************
 -=[ program ASCII ]=-

 wyswietla tablice znakow ASCII
 po najechaniu na znak wyswietla sie jego kod ASCII w roznych systemach liczbowych
 litery, cyfry i elementy tabelek sa zgrupowane wspolnym kolorem tla
 program dziala do czasu gdy uzytkownik nacisnie klawisz ESCAPE
 miedzy znakami mozna sie przemieszczac uzywajac klawiszy strzalek

 *************************/

#include <iostream>
#include <conio2.h>

using namespace std;

int main()
   {
   // wypisanie liczb u gory
   cout << "     ";
   for (int i=1; i < 33;i++)
      {
      if (i%2)
         textcolor(15);
      else
         textcolor(14);
      cout.width(2);
      cout << i;
      }

   // wypisanie tablicy znakow
   for (int i=1; i < 256; i++)
      {

      // ustawienie koloru tla po grupach znakow
      if ( (i==91) or (i==123) or (i==58) or (i==189) or (i==207)
         or (i==193) or (i==198) or (i==181) or (i==219))
         textbackground(BLACK);


      // nowa linia
      if (i%32==1)
         {
         cout << endl << endl;
         cout.width(4);
         cout << i-1 << ' ';
         }

      cout << ' ';

      // naprzemienne kolory znakow
      if (i%2)
         textcolor(15);
      else
         textcolor(14);

      // zmiana koloru tla dla liter
      if (i==65)
         textbackground(GREEN);
      if (i==97)
         textbackground(GREEN);
      // zmiana koloru tla dla cyfr
      if (i==48)
         textbackground(3);
      // zmiana koloru  dla elementow ramki grubej
      if ((i==185) or (i==200))
         textbackground(9);
      // zmiana koloru dla elementow ramki cienkiej
      if ((i==191) or (i==193) or (i==179) or (i==217))
         textbackground(5);

      // wypisuje tylko wizualne znaki
      if ( (i!=13) and (i!=10) and (i!=9) and (i!=8) and (i!=7) )
        putchar(i);
      else
        cout << ' ';
      }

   cout << endl << endl;
   cout << "     7 - sygnal dzwiekowy" << endl;
   cout << "     8 - backspace" << endl;
   cout << "     9 - tab" << endl;
   cout << "    10 - kursor do nastepnej linii" << endl;
   cout << "    13 - kursor na poczatek linii" << endl;

   int x = 7;
   int y = 3;
   gotoxy(x, y);
   _setcursortype(_SOLIDCURSOR);

   int k;
   while(k!=27)  // na klawisz ESCAPE wylacza program
      {
      if (kbhit()) // jezeli zostal nacisniety klawisz
         {
         k = getch(); // pobiera nr klawisza
         switch (k)
            {
            case 72:            // w gore
               if (y>3)
                  y = y - 2;
               break;
            case 80:            //w dol
               if (y<17)
                  y = y + 2;
               break;
            case 75:            //w lewo
               if (x>7)
                  x = x - 2;
               break;
            case 77:            //w prawo
               if (x<69)
                  x = x + 2;
               break;
            } // end switch

         // oblicza kod ASCII na podstawie wspolrzednych kursora
         int Liczba = (y-3)*16 + (x-5)/2;
         // wyswietla kod ASCII w roznych systemach liczbowych
         gotoxy(65,19);
         cout << "dec " << dec << Liczba << " ";
         gotoxy(65,21);
         cout << "hex " << hex << Liczba << " ";
         gotoxy(65,23);
         cout << "oct " << oct << Liczba << " ";

         gotoxy(x, y);

         } // end if
      } //end while
   } // end main
