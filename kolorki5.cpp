/*************************
 -=[ program kolorki 5 ]=-

 wyswietla kolorowe prostokaty w losowych miejscach

 *************************/
#include <iostream>
#include <conio2.h>

using namespace std;

// rysowanie prostokata
int Rect(int x1, int y1, int x2, int y2, int color=WHITE)
   {
   textbackground(color); // kolor prostokata
   for (int j = y1; j<=y2; j++)
      {
      gotoxy(x1, j);
      for (int i = x1; i<=x2; i++)
         {
         cout << " ";
         }
      }
   }

// rysowanie losowego prostokata
int RandRect()
   {
   int x1 = (rand()%60)+1;
   int x2 = rand()%19+x1+1;
   int y1 = (rand()%16)+1;
   int y2 = rand()%9+y1+1;
   Rect(x1, y1, x2, y2, rand()%16);
   _sleep(20);
   }

int main()
   {
   srand(time(NULL));

   while(!kbhit())
   {
   RandRect();
   }

   getch();
   getch();
   return 0;
   }
