#include <iostream>
#include <conio2.h>

using namespace std;

class TTrojkat
  {
  private:
    int DrawPelny(int X);
    int DrawPusty(int X);
    int CalcCenter(int X);
  public:
    int Left, Top;
    char MainChar, LeftChar, RightChar, BottomChar, InnerChar;
    bool Wypelniony, AutoCenter;
    int Draw(int X);
    TTrojkat();
  };

int TTrojkat::Draw(int X)
  {
  if (!X%2)
    {
    X++;
    }

  if(AutoCenter)
    CalcCenter(X);

  if (Wypelniony)
    DrawPelny(X);
  else
    DrawPusty(X);
  }

int TTrojkat::DrawPusty(int X)
  {
  int L = Left;
  int T = Top;

  int Poczatek = X / 2;
  int Szerokosc = 1;

  while (Poczatek >= 0)
    {
    gotoxy(L+Poczatek,T);

    if (Szerokosc > 1)
      cout << LeftChar;
    if (Poczatek > 0)
      {
      for(int i=2;i<Szerokosc;i++)
        cout << InnerChar;
      }
    else
      {
      for(int i=2;i<Szerokosc;i++)
        cout << BottomChar;
      }
    if (Szerokosc > 1)
      cout << RightChar;
    else
      cout << MainChar;

    Poczatek--;
    Szerokosc = Szerokosc + 2;
    T++;
    }
  }

int TTrojkat::DrawPelny(int X)
  {
  int L = Left;
  int T = Top;

  int Poczatek = X / 2;
  int Szerokosc = 1;

  while (Poczatek >= 0)
    {
    gotoxy(L+Poczatek,T);
    
    for(int i=1;i<=Szerokosc;i++)
      cout << MainChar;;

    Poczatek--;
    Szerokosc = Szerokosc + 2;
    T++;
    }
  }

int TTrojkat::CalcCenter(int X)
  {
  Left = (80 - X) / 2;
  Top = (25 - (X/2)+1) / 2;
  }

TTrojkat::TTrojkat()
  {
  Left = 1;
  Top = 1;
  MainChar = '*';
  LeftChar = '/';
  RightChar = '\\';
  BottomChar = '_';
  InnerChar = ' ';
  Wypelniony = true;
  AutoCenter = false;
  }

int main()
   {
   clrscr();

   TTrojkat Trojkat;

//   Trojkat.BorderColor = LIGHTRED;
//   Trojkat.InnerColor = LIGHTBLUE;
   Trojkat.MainChar = '+';
   Trojkat.BottomChar = '+';
   Trojkat.LeftChar = '+';
   Trojkat.RightChar = '+';
//   Trojkat.InnerChar = 219;
   Trojkat.Wypelniony = false;
   Trojkat.AutoCenter = true;
   Trojkat.Draw(41);
   Trojkat.Draw(31);
   Trojkat.Draw(21);
//   Trojkat.Draw(11);

   Trojkat.AutoCenter = false;
   Trojkat.Left = 2;
   Trojkat.Top = 2;
   Trojkat.Draw(5);

   Trojkat.Left = 2;
   Trojkat.Top = 22;
   Trojkat.Draw(5);

   Trojkat.Left = 75;
   Trojkat.Top = 22;
   Trojkat.Draw(5);

   Trojkat.Left = 75;
   Trojkat.Top = 2;
   Trojkat.Draw(4);


   Trojkat.MainChar = '*';
   Trojkat.BottomChar = '_';
   Trojkat.LeftChar = '/';
   Trojkat.RightChar = '\\';
   Trojkat.AutoCenter = true;
   Trojkat.Draw(11);


   getch();
   }

