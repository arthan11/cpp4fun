/*************************
 -=[ program kolorki 3 ]=-

 prezentuje na srodku napis
 tlem sa ramki ktorych kolor i kolor tekstu zmienia sie losowo
 program dziala do czasu gdy uzytkownik nacisnie dowolny klawisz

 *************************/
#include <iostream>
#include <conio2.h>

using namespace std;

const string TEKST = "NACISNIJ DOWOLNY KLAWISZ";
const int ILOSC_RAMEK = 12;  // max 25

int Napis(string txt)
    {
    textbackground(rand()%16);
    textcolor(rand()%16);  // ustawienie koloru tekstu
    gotoxy( (40 - txt.length()/2 ), 13); // przesuniecie kursora
    cout << txt << endl; // wypisanie napisu
    }

int RysujRamke(int x1,int y1,int x2,int y2)
   {
   cout.fill(' ');

   cout.width(x2-x1+1);
   gotoxy(x1,y1);
   cout << ' ';

   cout.width(x2-x1+1);
   gotoxy(x1,y2);
   cout << ' ';

   for (int i=y1+1; i<y2; i++)
      {
      gotoxy(x1,i);
      cout << ' ';
      gotoxy(x2,i);
      cout << ' ';
      }
   }


int RysujTlo(int ileramek)
   {
   clrscr();
   for (int i=0; i<ileramek; i++)
      {
      textbackground(rand()%16);  // ustawienie koloru tla
      RysujRamke(1+i,1+i,79-i,25-i);
      }
   }

int main()
    {
    srand(time(NULL));  // generator liczb losowych

    while(!kbhit()) // w petli do nacisniecia dowolnego klawisza
        {
        RysujTlo(ILOSC_RAMEK);
        Napis(TEKST);

        _sleep(50); // pauza
        }

    getch();
    getch();
    return 0;
    }
